using CommandLine;
using Xunit;

namespace Tests
{
  public class OptionTest
  {
    [Fact]
    public void CreateFromType()
    {
      var opt1 = Option.Create(typeof(Derived1));
      Assert.NotEmpty(opt1);
      Assert.Single(opt1);
      Assert.Collection(opt1, o =>
      {
        Assert.NotNull(o);
        Assert.IsType<Option>(o);
      });

      var opt2 = Option.Create(typeof(Derived2));
      Assert.Empty(opt2);
    }

    [Fact]
    public void CreateFromProperty()
    {
      var opt1 = Option.Create(typeof(Derived1).GetProperty(nameof(Derived1.Opt1)));
      Assert.NotNull(opt1);
      Assert.IsType<Option>(opt1);

      var opt2 = Option.Create(typeof(Derived2).GetProperty(nameof(Derived2.Opt2)));
      Assert.Null(opt2);
    }
  }
}
