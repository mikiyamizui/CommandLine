using CommandLine;
using Xunit;

namespace Tests
{
  public class ArgumentTest
  {
    [Fact]
    public void CreateFromType()
    {
      var args1 = Argument.Create(typeof(Derived1));
      Assert.NotEmpty(args1);
      Assert.Single(args1);
      Assert.Collection(args1, a =>
      {
        Assert.NotNull(a);
        Assert.IsType<Argument>(a);
      });

      var args2 = Argument.Create(typeof(Derived2));
      Assert.Empty(args2);
    }

    [Fact]
    public void CreateFromProperty()
    {
      var args1 = Argument.Create(typeof(Derived1).GetProperty(nameof(Derived1.Arg1)));
      Assert.NotNull(args1);
      Assert.IsType<Argument>(args1);

      var args2 = Argument.Create(typeof(Derived2).GetProperty(nameof(Derived2.Arg2)));
      Assert.Null(args2);
    }
  }
}
