using CommandLine;
using System.Reflection;
using Xunit;

namespace Tests
{
  public class CommandTest
  {
    [Fact]
    public void CreateFromAssembly()
    {
      var commands = Command.Create(Assembly.GetExecutingAssembly());
      Assert.NotEmpty(commands);
      Assert.Single(commands);
    }
  }
}
