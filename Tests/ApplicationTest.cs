using System.Reflection;
using CommandLine;
using Xunit;

namespace Tests
{
    public class ApplicationTest
    {
        [Fact]
        public void CreateApplicationFromExecutingAssembly()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var app = Application.Create(assembly);

            // Assert.NotNull(app);
            Assert.IsType<Application>(app);

            Assert.Single(app.Arguments);
            Assert.Single(app.Options);
        }
    }
}