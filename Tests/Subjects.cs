using CommandLine;

namespace Tests
{
  [Command]
  internal abstract class Base1
  {
    [Argument]
    public int Arg1 { get; set; }

    [Option("-a|--aaa")]
    public bool Opt1 { get; set; }

    public virtual void Run() { }
  }

  [Command(Inheritable = false)]
  internal abstract class Base2
  {
    [Argument(Inheritable = false)]
    public int Arg2 { get; set; }

    [Option("-b|--bbb", Inheritable = false)]
    public bool Opt2 { get; set; }

    public virtual void Run() { }
  }

  internal class Derived1 : Base1 { }

  internal class Derived2 : Base2 { }
}