﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sample
{
  [Command]
  internal abstract class CommandBase
  {
    [Argument]
    public List<int> Values { get; set; }

    [Option("--bool|-b", "This is description of option1")]
    public bool BoolOption { get; set; }

    [Option("-s|--single", "This is description of option1")]
    public bool SingleValue { get; set; }

    [Option("-m|--multi", "This is description of option1")]
    public bool MultiValues { get; set; }

    public abstract double Result { get; }

    public void Run()
    {
      var v = string.Join(", ", Values);
      Console.WriteLine($"[{v}] => {Result}");
    }
  }

  internal class AddCommand : CommandBase
  {
    public override double Result =>
      Values.Aggregate(0.0, (result, value) => result + value);
  }

  internal class MulCommand : CommandBase
  {
    public override double Result =>
      Values.Aggregate(1.0, (result, value) => result * value);
  }

  internal static class Program
  {
    private static void Main(string[] args)
    {
      var app = Application.Create();
      app.Description = "This is sample app.";
      if (!app.Run(args))
      {
        app.ShowHelp();
      }
    }
  }
}
