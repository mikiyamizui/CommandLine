namespace CommandLine
{
  public enum HelpStyle : int
  {
    None = 0,
    Usage = 1,
    Simplified = 2,
    Detailed = 4,
  }
}