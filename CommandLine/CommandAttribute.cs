using CommandLine.Abstractions;
using System;

namespace CommandLine
{
  [AttributeUsage(AttributeTargets.Class)]
  public class CommandAttribute : Attribute, IName, IDescription, IInheritable
  {
    public string Name { get; set; }

    public string Description { get; set; }

    public string Method { get; set; }

    public bool Inheritable { get; set; } = true;

    public CommandAttribute() { }

    public CommandAttribute(string name)
    {
      Name = name;
    }

    public CommandAttribute(string name, string description)
    {
      Name = name;
      Description = description;
    }

    public CommandAttribute(string name, string description, string method)
    {
      Name = name;
      Description = description;
      Method = method;
    }
  }
}