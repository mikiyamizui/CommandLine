namespace CommandLine
{
  public enum OptionType
  {
    NoValue,
    SingleValue,
    MultipleValues,
  }
}