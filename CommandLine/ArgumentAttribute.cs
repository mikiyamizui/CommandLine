using CommandLine.Abstractions;
using System;

namespace CommandLine
{
  [AttributeUsage(AttributeTargets.Property)]
  public class ArgumentAttribute : Attribute, IName, IDescription, IInheritable
  {
    public string Name { get; set; }

    public string Description { get; set; }

    public bool Optional { get; set; }

    public bool Inheritable { get; set; } = true;

    public ArgumentAttribute() { }

    public ArgumentAttribute(string name)
    {
      Name = name;
    }

    public ArgumentAttribute(string name, string description)
    {
      Name = name;
      Description = description;
    }

    public ArgumentAttribute(string name, string description, bool optional)
    {
      Name = name;
      Description = description;
      Optional = optional;
    }
  }
}