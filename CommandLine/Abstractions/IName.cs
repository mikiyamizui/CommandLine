namespace CommandLine.Abstractions
{
  public interface IName
  {
    string Name { get; }
  }
}