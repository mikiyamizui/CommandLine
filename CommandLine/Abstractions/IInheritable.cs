namespace CommandLine
{
  public interface IInheritable
  {
    bool Inheritable { get; }
  }
}