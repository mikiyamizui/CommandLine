using System.Collections.Generic;

namespace CommandLine.Abstractions
{
  public interface ICommand : IName, IDescription
  {
    IEnumerable<ICommand> Commands { get; }

    IEnumerable<IArgument> Arguments { get; }

    IEnumerable<IOption> Options { get; }

    string HelpOption { get; }

    bool Run(IEnumerable<string> args);

    void ShowHelp();

    void ShowHelp(HelpStyle commandStyle, HelpStyle argumentStyle, HelpStyle optionStyle);
  }
}
