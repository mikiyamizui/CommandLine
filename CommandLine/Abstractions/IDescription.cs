namespace CommandLine.Abstractions
{
  public interface IDescription
  {
    string Description { get; }
  }
}