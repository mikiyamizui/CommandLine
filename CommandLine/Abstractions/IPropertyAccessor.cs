using System;
using System.Reflection;

namespace CommandLine.Abstractions
{
  public interface IPropertyAccessor
  {
    PropertyInfo Property { get; }

    Type ValueType { get; }

    bool MultipleValues { get; }

    int Count { get; }

    void Supply(ICommand command, object instance, string value);
  }
}
