using CommandLine.Abstractions;

namespace CommandLine
{
  public interface IOption : IPropertyAccessor, IName, IDescription
  {
    OptionType OptionType { get; }

    string Value { get; }
  }
}