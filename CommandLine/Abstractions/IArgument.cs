using CommandLine.Abstractions;

namespace CommandLine
{
  public interface IArgument : IPropertyAccessor, IName, IDescription, IOptional
  {
  }
}