using CommandLine.Abstractions;
using System;

namespace CommandLine
{
  [AttributeUsage(AttributeTargets.Property)]
  public class OptionAttribute : Attribute, IName, IDescription, IInheritable
  {
    public string Name { get; set; }

    public string Value { get; set; }

    public string Description { get; set; }

    public bool Inheritable { get; set; } = true;

    public OptionAttribute(string name)
    {
      Name = name;
    }

    public OptionAttribute(string name, string description)
    {
      Name = name;
      Description = description;
    }

    public OptionAttribute(string name, string value, string description)
    {
      Name = name;
      Value = value;
      Description = description;
    }
  }
}