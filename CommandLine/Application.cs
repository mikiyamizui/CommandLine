using CommandLine.Abstractions;
using CommandLine.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CommandLine
{
  public class Application : ICommand
  {
    public static Application Current { get; private set; }

    public Assembly Assembly { get; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string Version { get; set; }

    public IEnumerable<ICommand> Commands { get; } = new List<ICommand>();

    public IEnumerable<IArgument> Arguments => Commands
      .SelectMany(command => command.Arguments)
      .GroupBy(argument => argument.Name)
      .Select(group => group.First());

    public IEnumerable<IOption> Options => Commands
      .SelectMany(command => command.Options)
      .GroupBy(option => option.Name)
      .Select(group => group.First());

    public Action<string, ICommand> OnError { get; set; } = (msg, command) =>
    {
      Console.Error.WriteLine(msg);
      Console.Error.WriteLine();
    };

    public string HelpOption { get; set; } = "-h|--help";

    public string VersionOption { get; set; } = "-v|--version";

    public static Application Create()
      => Application.Create(Assembly.GetEntryAssembly());

    public static Application Create(Assembly assembly)
    {
      var commands = Command.Create(assembly);
      return new Application(assembly, commands);
    }

    internal Application(Assembly assembly, IEnumerable<Command> commands)
    {
      if (Current != null)
      {
        throw new CommandLineException("Multiple application instance are not supported.", this);
      }

      if (!commands.Any())
      {
        throw new CommandLineException("No command is defined.", this);
      }

      Current = this;
      Assembly = assembly;
      Commands = commands;
      Name = Path.GetFileName(assembly.Location);
      Version = assembly
        .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
        .InformationalVersion;
    }

    public bool Run(IEnumerable<string> args)
    {
      try
      {
        if (HelpUtil.TryShowHelp(this, args) || HelpUtil.TryShowVersion(this, args))
        {
          return true;
        }

        if (!Commands.Skip(1).Any())
        {
          return Commands.Single().Run(args);
        }

        var name = args.FirstOrDefault();
        if (!string.IsNullOrEmpty(name))
        {
          foreach (var command in Commands)
          {
            if (NameUtil.Match(command, name))
            {
              if (command.Run(args.Skip(1)))
              {
                return true;
              }
            }
          }

          OnError($"Command '{name}' was not found.", this);
        }
      }
      catch (CommandLineException error)
      {
        OnError(error.Message, error.Application);
      }

      return false;
    }

    public void ShowHelp()
      => ShowHelp(
        commandStyle: Commands.Any()
          && Commands.All(c => !string.IsNullOrEmpty(c.Description))
          ? HelpStyle.Detailed : HelpStyle.Simplified,
        argumentStyle: Arguments.Any()
          && Arguments.All(a => !string.IsNullOrEmpty(a.Description))
          ? HelpStyle.Detailed : HelpStyle.Usage,
        optionStyle: Options.Any()
          && Options.All(a => !string.IsNullOrEmpty(a.Description))
          ? HelpStyle.Detailed : HelpStyle.Simplified
      );

    public void ShowHelp(HelpStyle commandStyle, HelpStyle argumentStyle, HelpStyle optionStyle)
    {
      if (!Commands.Skip(1).Any())
      {
        var command = Commands.Single();
        HelpUtil.ShowHelp(this,
          command.Commands, command.Arguments, command.Options,
          commandStyle, argumentStyle, optionStyle);
      }
      else
      {
        HelpUtil.ShowHelp(this,
          Commands, Arguments, Options,
          commandStyle, argumentStyle, optionStyle);
      }
    }
  }
}