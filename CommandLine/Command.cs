using CommandLine.Abstractions;
using CommandLine.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CommandLine
{
  public class Command : ICommand
  {
    public Type TargetType { get; }

    public MethodInfo Method { get; }

    public string Name { get; }

    public string Description { get; }

    public IEnumerable<ICommand> Commands { get; } = new List<ICommand>();

    public IEnumerable<IArgument> Arguments { get; } = new List<IArgument>();

    public IEnumerable<IOption> Options { get; } = new List<IOption>();

    public string HelpOption => Application.Current.HelpOption;

    public static IEnumerable<Command> Create() => Create(Assembly.GetEntryAssembly());

    public static IEnumerable<Command> Create(Assembly assembly)
      => assembly.GetTypes()
        .Where(type => !type.IsAbstract && type.IsClass && !type.IsGenericType)
        .Select(type => Command.Create(type) as Command)
        .OfType<Command>()
        .ToList();

    public static Command Create<T>() => Create(typeof(T));

    public static Command Create(Type type)
    {
      var attr1 = type.GetCustomAttribute<CommandAttribute>(true);
      var attr2 = type.GetCustomAttribute<CommandAttribute>(false);
      if (attr1 == null || (!attr1.Inheritable && attr1 != attr2))
      {
        return null;
      }

      var name = attr1?.Name;
      if (string.IsNullOrEmpty(name))
      {
        name = type.Name.ToLower();
        if (name.EndsWith("command"))
        {
          name = name.Substring(0, name.IndexOf("command"));
        }
      }
      if (string.IsNullOrEmpty(name))
      {
        throw new CommandLineException($"Type '{type.Name}' does not have any command name.");
      }

      var method = type.GetMethod(attr1?.Method ?? "Run", BindingFlags.Instance | BindingFlags.Public);
      if (method == null)
      {
        throw new CommandLineException($"Type '{type.Name}' does not have any Run method.");
      }

      var arguments = Argument.Create(type);
      var options = Option.Create(type);
      string description = attr1?.Description ?? "";

      return new Command(type, method, name, description, arguments, options);
    }

    internal Command(Type type, MethodInfo method, string name, string description,
      IEnumerable<IArgument> arguments, IEnumerable<IOption> options)
    {
      TargetType = type;
      Method = method;
      Name = name;
      Description = description;
      Arguments = arguments;
      Options = options;
    }

    public bool Run(IEnumerable<string> args)
    {
      if (HelpUtil.TryShowHelp(this, args))
      {
        return true;
      }

      var instance = Activator.CreateInstance(TargetType);
      for (int i = 0; args.Skip(i).Any(); i++)
      {
        var s = args.ElementAt(i);

        var option = Options.SingleOrDefault(o => NameUtil.Match(o, s));
        if (option != null)
        {
          option.Supply(this, instance, s);
          continue;
        }

        var argument = Arguments.FirstOrDefault(p => p.MultipleValues || p.Count == 0);
        if (argument == null)
        {
          break;
        }

        argument.Supply(this, instance, s);
      }

      var unsupplied = Arguments.Where(a => !a.Optional && a.Count == 0);
      if (unsupplied.Any())
      {
        var names = string.Join(", ", unsupplied.Select(a => a.Name));
        throw new CommandLineException($"Missing argument: {names}", this);
      }

      Method.Invoke(instance, null);
      return true;
    }

    public void ShowHelp()
      => ShowHelp(
        commandStyle: Commands.Any() && Commands.All(c => !string.IsNullOrEmpty(c.Description))
          ? HelpStyle.Detailed : HelpStyle.Simplified,
        argumentStyle: Arguments.Any() && Arguments.All(a => !string.IsNullOrEmpty(a.Description))
          ? HelpStyle.Detailed : HelpStyle.Usage,
        optionStyle: Options.Any() && Options.All(a => !string.IsNullOrEmpty(a.Description))
          ? HelpStyle.Detailed : HelpStyle.Simplified
        );

    public void ShowHelp(HelpStyle commandStyle, HelpStyle argumentStyle, HelpStyle optionStyle)
      => HelpUtil.ShowHelp(this, Commands, Arguments, Options, commandStyle, argumentStyle, optionStyle);
  }
}