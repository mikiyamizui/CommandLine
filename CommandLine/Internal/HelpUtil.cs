using CommandLine.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandLine.Internal
{
  internal static class HelpUtil
  {
    public static bool TryShowVersion(Application app, IEnumerable<string> args)
    {
      if (app.VersionOption
        .Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries)
        .Any(versionOption => args.Contains(versionOption)))
      {
        ShowVersion();
        return true;
      }
      return false;
    }

    public static bool TryShowHelp(ICommand command, IEnumerable<string> args)
    {
      if (command.HelpOption
        .Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries)
        .Any(helpOption => args.Contains(helpOption)))
      {
        command.ShowHelp();
        return true;
      }
      return false;
    }

    public static void ShowVersion()
    {
      var sb = new StringBuilder();
      sb.Append(Application.Current.Name);
      if (!string.IsNullOrEmpty(Application.Current.Version))
      {
        sb.Append(" ");
        sb.Append(Application.Current.Version);
      }

      Console.WriteLine(sb.ToString());
    }

    public static void ShowHelp(
      ICommand command,
      IEnumerable<ICommand> commands,
      IEnumerable<IArgument> arguments,
      IEnumerable<IOption> options,
      HelpStyle commandStyle,
      HelpStyle argumentStyle,
      HelpStyle optionStyle)
    {
      var help = new Option(null, command.HelpOption, "true", "Show help");
      options = options.Concat(new[] { help });

      if (command is Application app)
      {
        var version = new Option(null, app.VersionOption, "true", "Show version");
        options = options.Concat(new[] { version });
      }

      ShowVersion();

      var sb = new StringBuilder();

      if (!string.IsNullOrEmpty(command.Description))
      {
        sb.AppendLine(command.Description);
      }

      sb.Append("Usage: ");
      if (command != Application.Current)
      {
        sb.Append(Application.Current.Name);
        sb.Append(" ");
      }

      sb.Append(command.Name);

      if (commands.Any())
      {
        if (commandStyle.HasFlag(HelpStyle.Usage))
        {
          sb.Append(" <");
          sb.Append(string.Join("|", commands.Select(c => c.Name)));
          sb.Append(">");
        }
        else
        {
          sb.Append(" <COMMAND>");
        }
      }

      if (options.Any())
      {
        if (optionStyle.HasFlag(HelpStyle.Usage))
        {
          foreach (var option in options)
          {
            var names = NameUtil.Split(option);
            foreach (var name in names)
            {
              sb.Append(" ");
              switch (option.OptionType)
              {
                case OptionType.NoValue:
                  sb.Append($"[{name}]");
                  break;
                case OptionType.SingleValue:
                  sb.Append($"[{name} <{option.Value}>]");
                  break;
                case OptionType.MultipleValues:
                  sb.Append($"[{name} <{option.Value}>...]");
                  break;
              }
            }
          }
        }
        else
        {
          sb.AppendFormat(" [OPTION{0}]", arguments.Skip(1).Any() ? "S..." : "");
        }
      }

      if (arguments.Any())
      {
        if (argumentStyle.HasFlag(HelpStyle.Usage))
        {
          foreach (var argument in arguments)
          {
            sb.Append(" ");
            var optional = argument.Optional || argument.MultipleValues ? "[]" : null;
            var multiple = argument.MultipleValues ? "..." : null;
            sb.Append($"{optional?.ElementAt(0)}<{argument.Name}{multiple}>{optional?.ElementAt(1)}");
          }
        }
        else
        {
          sb.AppendFormat(" <ARG{0}>", arguments.Skip(1).Any() ? "S..." : "");
        }
      }
      sb.AppendLine();

      ShowHelpItems(sb, "Commands", commands, commandStyle);
      ShowHelpItems(sb, "Arguments", arguments, argumentStyle);
      ShowHelpItems(sb, "Options", options, optionStyle);

      Console.WriteLine(sb.ToString());
    }

    private static void ShowHelpItems<T>(StringBuilder sb,
      string caption, IEnumerable<T> items, HelpStyle style)
      where T : IName
    {
      if (items.Any())
      {
        if (style.HasFlag(HelpStyle.Detailed))
        {
          sb.AppendLine($"{caption}:");
          var width = items.Max(item => NameUtil.Normalize(item).Length);
          foreach (var item in items)
          {
            sb.Append("  ");
            sb.Append(NameUtil.Normalize(item).PadRight(width));
            if (item is IDescription desc && !string.IsNullOrEmpty(desc.Description))
            {
              sb.Append($"    {desc.Description}");
            }

            sb.AppendLine();
          }
        }
        else if (style.HasFlag(HelpStyle.Simplified))
        {
          sb.Append($"{caption}: ");

          var help = items.Where(item => NameUtil.Match(item, Application.Current.HelpOption));
          var version = items.Where(item => NameUtil.Match(item, Application.Current.VersionOption));
          sb.AppendLine(string.Join(", ", items
            .Except(help.Concat(version))
            .SelectMany(item => NameUtil.Split(item))
            .OrderBy(name => name)
            .Concat(help.Concat(version).SelectMany(item => NameUtil.Split(item)))
          ));
        }
      }
    }
  }
}