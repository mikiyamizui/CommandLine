using CommandLine.Abstractions;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace CommandLine.Internal
{
  public abstract class PropertyAccessor : IPropertyAccessor, IName
  {
    public PropertyInfo Property { get; }

    public Type ValueType { get; }

    public bool MultipleValues { get; }

    public int Count { get; private set; }

    public string Name { get; protected set; }

    protected PropertyAccessor(string name)
    {
      Name = name;
    }

    protected PropertyAccessor(PropertyInfo property, string name)
      : this(name)
    {
      Property = property;

      var type = property?.PropertyType;
      MultipleValues = property?.PropertyType.GetInterface(typeof(ICollection<>).Name) != null;

      ValueType = property?.PropertyType;
      if (MultipleValues)
      {
        if (type.IsGenericType)
        {
          var i = type.GetInterface(typeof(IEnumerable<>).Name);
          if (i != null)
          {
            ValueType = property?.PropertyType.GetGenericArguments()[0];
          }
        }
        else
        {
          throw new CommandLineException($"Generic type property is not supported.");
        }
      }
    }

    public virtual void Supply(ICommand command, object instance, string value)
    {
      try
      {
        if (Property != null)
        {
          var valueOrList = Property.GetValue(instance) ?? Activator.CreateInstance(Property.PropertyType);

          if (MultipleValues)
          {
            var add = Property.PropertyType.GetMethod("Add");
            add.Invoke(valueOrList, new[] { Convert.ChangeType(value, ValueType) });
          }
          else
          {
            valueOrList = Convert.ChangeType(value, ValueType);
          }

          Property.SetValue(instance, valueOrList);

          Count++;
        }
      }
      catch (Exception error)
      {
        throw new CommandLineException($"'{value}' can not used for the argument: {Name}", error);
      }
    }
  }
}