using CommandLine.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CommandLine.Internal
{
  public static class NameUtil
  {
    public static string Normalize(IName obj)
      => string.Join(", ", Split(obj).OrderBy(name => name));

    public static string Normalize(string name)
      => string.Join(", ", Split(name).OrderBy(str => str));

    public static IEnumerable<string> Split(IName obj)
      => Split(obj.Name);

    public static IEnumerable<string> Split(string name)
      => name.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

    public static bool Match(IName obj, string name)
      => Match(obj.Name, name);

    public static bool Match(string name1, string name2)
      => name1.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries)
        .Intersect(name2.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries)
        .Select(s => s.ToLower())).Any();
  }
}