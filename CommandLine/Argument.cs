using CommandLine.Abstractions;
using CommandLine.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CommandLine
{
  public class Argument : PropertyAccessor, IArgument, IOptional
  {
    public string Description { get; }

    public bool Optional { get; }

    public static IEnumerable<IArgument> Create(Type desclaringType)
    {
      var argument = desclaringType
        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
        .Where(p => p.CanRead && p.CanWrite)
        .Select(p => Argument.Create(p))
        .OfType<IArgument>()
        .ToList()
        ;
      return argument;
    }

    public static IArgument Create(PropertyInfo property)
    {
      var attr = property.GetCustomAttribute<ArgumentAttribute>();
      if (attr == null || (!attr.Inheritable && property.DeclaringType != property.ReflectedType))
      {
        return null;
      }

      var name = (attr?.Name ?? property.Name).ToLower();
      if (string.IsNullOrEmpty(name))
      {
        throw new CommandLineException($"Argument '{property.DeclaringType.Name}.{property.Name}' does not have any proper name.");
      }

      var description = attr?.Description ?? "";
      var optional = attr?.Optional ?? false;
      return new Argument(property, name, description, optional);
    }

    public Argument(string name, string description, bool optional)
      : base(null, name)
    {
      Description = description;
      Optional = optional;
    }

    public Argument(PropertyInfo property, string name, string description, bool optional)
      : base(property, name)
    {
      Description = description;
      Optional = optional;
    }
  }
}