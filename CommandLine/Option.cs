using CommandLine.Abstractions;
using CommandLine.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CommandLine
{
  public class Option : PropertyAccessor, IOption
  {
    public string Description { get; }

    public OptionType OptionType { get; }

    public string Value { get; }

    public static IEnumerable<IOption> Create(Type desclaringType)
      => desclaringType
        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
        .Where(p => p.CanRead && p.CanWrite)
        .Select(p => Option.Create(p))
        .OfType<IOption>()
        .ToList();

    public static IOption Create(PropertyInfo property)
    {
      var attr = property.GetCustomAttribute<OptionAttribute>();
      if (attr == null || (!attr.Inheritable && property.DeclaringType != property.ReflectedType))
      {
        return null;
      }

      var name = attr?.Name;
      if (string.IsNullOrEmpty(name))
      {
        throw new CommandLineException($"Option '{property.DeclaringType.Name}.{property.Name}' does not have any proper name.");
      }

      var value = attr?.Value ?? property.Name;

      var description = attr?.Description ?? "";
      return new Option(property, name, value, description);
    }

    public Option(string name, string value, string description)
      : base(name)
    {
      Description = description;
      Value = value;
    }

    public Option(PropertyInfo property, string name, string value, string description)
      : base(property, name)
    {
      Description = description;
      Value = value;
      OptionType = MultipleValues ? OptionType.MultipleValues
        : (property?.PropertyType == typeof(bool) ? OptionType.NoValue : OptionType.SingleValue);
    }

    public override void Supply(ICommand command, object instance, string value)
      => base.Supply(command, instance, OptionType == OptionType.NoValue ? "true" : value);
  }
}