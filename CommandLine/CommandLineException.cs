using CommandLine.Abstractions;
using System;

namespace CommandLine
{
  public class CommandLineException : Exception
  {
    public ICommand Application { get; }

    public CommandLineException(string message) : base(message) { }

    public CommandLineException(string message, Exception error) : base(message, error) { }

    public CommandLineException(string message, ICommand app) : base(message)
    {
      Application = app;
    }
  }
}